module.exports = {
  name: "default",
  type: "postgres",
  host: "localhost",
  port: 5432,
  username: "postgres",
  password: "postgres",
  database: "oishii_dev",
  entities: "entities/**/*.ts",
  migrations: "migrations/**/*.ts"
};
