import Chance from "chance";
import { getRepository } from "typeorm";
import { Recipe } from "../entities/Recipe";

const chance = new Chance();

export class RecipeFactory {
  static build(attrs: Partial<Recipe>) {
    const recipeAttrs: Partial<Recipe> = {
      title: chance.sentence(),
      ...attrs
    };

    return getRepository(Recipe).create(recipeAttrs);
  }

  static async create(attrs: Partial<Recipe>) {
    const recipe = RecipeFactory.build(attrs);
    const createdRecipe = await getRepository(Recipe).save(recipe);

    return createdRecipe;
  }

  static async deleteAll() {
    await getRepository(Recipe).query(`truncate "recipes" cascade`);
  }
}
