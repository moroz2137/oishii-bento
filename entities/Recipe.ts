import {
  BeforeInsert,
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from "typeorm";
import slugify from "../lib/helpers/slugify";

@Entity("recipes")
export class Recipe {
  @PrimaryGeneratedColumn("uuid")
  public id!: string;

  @Column({ type: "varchar" })
  public title!: string;

  @BeforeInsert()
  setSlug() {
    if (!this.slug?.trim() && this.title.trim()) {
      this.slug = slugify(this.title);
    }
  }

  @Column({ type: "varchar", unique: true })
  public slug!: string;

  @Column({ type: "text", nullable: true })
  public ingredients!: string | null;

  @Column({ type: "text" })
  public instructions!: string;

  @Column({ type: "timestamp", nullable: true })
  public publishedAt!: Date | null;

  @CreateDateColumn()
  public createdAt!: Date;

  @UpdateDateColumn()
  public updatedAt!: Date;

  public toJSON() {
    return {
      ...this,
      createdAt: this.createdAt?.toString(),
      updatedAt: this.updatedAt?.toString(),
      publishedAt: this.publishedAt?.toString() || null
    };
  }
}
