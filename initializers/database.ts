import "reflect-metadata";
import {
  Connection,
  ConnectionOptions,
  createConnection,
  getConnectionManager,
  getConnectionOptions
} from "typeorm";
import { Recipe } from "../entities/Recipe";
import { SnakeNamingStrategy } from "typeorm-naming-strategies";

// https://github.com/vercel/next.js/discussions/12254
const CONNECTION_ATTEMPT_INTERVAL = 100;
const CONNECTION_TIMEOUT = 3;

let isConnecting = false;
let connection: Connection;

const initializeDatabase = async () => {
  if (isConnecting) return;
  isConnecting = true;

  const connections = getConnectionManager();
  if (connections.has("default")) {
    await connections.get("default").close();
  }

  const connectionOptions = await getConnectionOptions();
  const options: ConnectionOptions = {
    ...connectionOptions,
    entities: [Recipe],
    migrations: [`${__dirname}/migrations/*.ts`],
    namingStrategy: new SnakeNamingStrategy()
  };

  connection = await createConnection(options);
  isConnecting = false;

  console.log(`Connection "default" initialized.`);
};

// run initialization on script execution.
// for prod this will only happen once, but for dev this will happen every time this module is hot reloaded
initializeDatabase();

export const connect = async () => {
  let waiting = 0;
  while (!connection) {
    await new Promise((resolve) =>
      setTimeout(resolve, CONNECTION_ATTEMPT_INTERVAL)
    );
    waiting += CONNECTION_ATTEMPT_INTERVAL;
    if (waiting > CONNECTION_TIMEOUT) break;
  }
  if (!connection) throw new Error("Database not initialized");
  return connection;
};

export default initializeDatabase;
