import { getRepository } from "typeorm";
import { Recipe } from "../../entities/Recipe";
import { connect } from "../../initializers/database";

export default class Recipes {
  static async listRecipesForSSR() {
    await connect();
    const recipes = await getRepository(Recipe)
      .createQueryBuilder()
      .select("r.*")
      .from(Recipe, "r")
      .where("r.published_at is not null")
      .execute();
    return JSON.parse(JSON.stringify(recipes));
  }
}
