import initializeDatabase from "../initializers/database";
import { RecipeFactory } from "../recipes/RecipeFactory";

initializeDatabase().then(() => {
  RecipeFactory.create({
    title: "Onion with stuff",
    instructions: "Mix onions, garlic, cucumbers, and add stuff.",
    ingredients: "- 1 onion\n- 1 clove garlic\n- turmeric"
  });
});
