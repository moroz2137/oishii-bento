import Head from "next/head";
import { Recipe } from "../entities/Recipe";
import Recipes from "../lib/contexts/Recipes";
import styles from "../styles/Home.module.css";

interface Props {
  recipes: Recipe[];
}

export default function Home({ recipes }: Props) {
  return (
    <div className={styles.container}>
      <Head>
        <title>美味しい弁当</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <h1>美味しい弁当</h1>
      <p>
        「美味しい弁当」は簡単なレシピのサイトだ、僕に日本語の練習や Next.js
        の勉強のためにされました。
      </p>
      <p>
        美味しい料理に興味があったら、以下のレシピを見てご覧。そして、自分のレシピを共有したかったら、あなたのレシピをサイトに新規登録してください。
      </p>
      <pre>{JSON.stringify(recipes, null, 2)}</pre>
    </div>
  );
}

export async function getServerSideProps() {
  const recipes = await Recipes.listRecipesForSSR();
  return {
    props: { recipes }
  };
}
