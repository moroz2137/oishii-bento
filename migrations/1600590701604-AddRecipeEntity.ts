import { MigrationInterface, QueryRunner, Table, TableIndex } from "typeorm";

export class AddRecipeEntity1600590701604 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `create extension if not exists "uuid-ossp" with schema public`
    );

    await queryRunner.createTable(
      new Table({
        name: "recipes",
        columns: [
          {
            name: "id",
            type: "uuid",
            isPrimary: true,
            default: "uuid_generate_v4()"
          },
          { name: "slug", type: "varchar", isNullable: false },
          { name: "title", type: "varchar", isNullable: false },
          { name: "ingredients", type: "text", isNullable: true },
          { name: "instructions", type: "text", isNullable: false },
          { name: "published_at", type: "timestamp", isNullable: true },
          {
            name: "created_at",
            type: "timestamp",
            default: "(now() at time zone 'utc')"
          },
          {
            name: "updated_at",
            type: "timestamp",
            default: "(now() at time zone 'utc')"
          }
        ]
      })
    );

    await queryRunner.createIndex(
      "recipes",
      new TableIndex({ columnNames: ["title"], name: "recipes_title_index" })
    );

    await queryRunner.createIndex(
      "recipes",
      new TableIndex({
        columnNames: ["slug"],
        isUnique: true,
        name: "recipes_slug_index"
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropIndex("recipes", "recipes_title_index");
    await queryRunner.dropIndex("recipes", "recipes_slug_index");
    await queryRunner.dropTable("recipes");
  }
}
